/**
 * index.js - Message scrapper
 * @version 0.1
 * @author mtyszczak
 */

// Import required modules
import process from "process";
import fs from "fs";
import dot from "dotenv";
import request from "sync-request";
dot.config();

// Save init timestamp for later logging
const initTimestamp = Date.now();

// Make out directory if does not exist
fs.mkdir("out", { recursive: true }, e => (e!==null) ? console.error(`'out' path creation error: ${e.message}`) : 0 );

// Configure envs
const msgLimit = ~~process.env.MESSAGE_BUFFER;
const channelID = process.env.CHANNEL_ID;
const auth = process.env.AUTH_HEADER;
const urlPrefix = `https://discord.com/api/v9/channels/${channelID}`; // Basic Discord channel if prefix

const wrongEnvCheck = ( envName, envVal ) => {
  if( (typeof envVal === "string" && envVal.length > 0) // We allow string and number values
   || (typeof envVal === "number" && !isNaN(envVal))
  ) return;

  console.warn( `Environment variable ${envName} has invalid value. Make sure you know what you are doing. ${envName}:`, envVal )
};
wrongEnvCheck( "MESSAGE_BUFFER", msgLimit );
wrongEnvCheck( "CHANNEL_ID", channelID );
wrongEnvCheck( "AUTH_HEADER", auth );

let tempArray = [{}], lastMessage = 0;

// Get channel info
const channelResponse = request("GET", urlPrefix, {
    headers: {
      "Authorization": auth,
    },
  }).getBody().toString();
const channel = JSON.parse( channelResponse );

let outPrefix = channel.parent_id || channel.id;
outPrefix += '_';
outPrefix += channel.position || "0";

// Open file for output
const stream = fs.createWriteStream( `out/${outPrefix}_scrap.json` );
stream.once("open", _fd => {

  console.info( `Scrapping from the channel "${channel.name || channel.recipients[0].username}"` );

  stream.write("{\n");
  stream.write(`  "channel_data":${channelResponse},\n`); // Write channel info to the stream

  let i = 0, sum = 0;
  // Iterate through the channel messages until msgLimit is less than array size
  do {
    let url = `${urlPrefix}/messages?limit=${msgLimit}`;
    if( lastMessage ) // Set where to start
      url += `&before=${lastMessage}`;

    const result = request("GET", url, {
      headers: {
        "Authorization": auth,
      },
    });

    const str = result.getBody().toString();
    tempArray = JSON.parse( str );
    sum += tempArray.length;
    if( tempArray.length ) // Save last message id
      lastMessage = tempArray[ tempArray.length - 1 ].id;

    // Save messages to the file
    stream.write( `  "${i}":` );
    stream.write( str );
    stream.write( `,\n` );
    ++i;
    console.log( `Iteration #${i}. Total sum of messages scrapped: ${sum}` );
  } while( tempArray.length === msgLimit );

  stream.write(`  "${i}":[]\n}`);

  console.info( `Processing channel "${channel.name || channel.recipients[0].username}" took ${~~((Date.now()-initTimestamp)/1000)} seconds` );

  stream.close();
});
