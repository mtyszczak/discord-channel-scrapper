# discord-channel-scrapper

[![License GNU GPL v3](https://img.shields.io/badge/License-GNU%20GPL%20v3-red.svg)](LICENSE.md)
[![Version 0.1](https://img.shields.io/badge/Version-0.1-abc.svg)](https://gitlab.com/jewcash/discord-channel-scrapper/-/commits/master)

Discord channel scrapper (info and messages) - works with group chats, DMs and server channels

**Note: Use at your own risk! This program is for illustration purposes only!**

## Configuring
Before running the script you have to rename `.env.example` file to the `.env` and fill all of the fields with proper values.

## Running
After [configuring](#Configuring) you can the script with:
```bash
node src
```
Output will be saved to the `out` directory (if it does not exist, program will create it automatically).

## License
Distributed under the GNU GENERAL PUBLIC LICENSE Version 3
See [LICENSE.md](LICENSE.md)